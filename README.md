This repository contains a template that you can use as a starting point to implement your own nature recognition algorithm, generating analysis results in a format that can be used directly by ARISE DSI.

See the [ARISE DSI algorithm development guidelines](https://docs.google.com/document/d/1AjJSUXoVktqeaqf0_7ZK8ka4pM1t-dPtdbDBRXL5-qs/edit) for more information and examples. The [ARISE machine prediction API](https://docs.google.com/document/d/1TQJtAJaEib5KZSNE6RFeg31QH1X_efiKw0KiB9RLvGI/edit) contains additional technical documentation. Please contact the ARISE DSI team if you have any questions.

To build the Docker container:

    docker build . -t arise-detection

To start the container:

    docker run -p 5000:5000 arise-detection

To get ready for testing your algorithm's output, install the environment:

    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements.test.txt
    git clone "https://gitlab.com/arise-biodiversity/schemas.git" tests/schemas

To test your API, including JSON schema checking, make sure the Docker container has been started (as above) and then run:

    source venv/bin/activate
    pytest --capture=no

